import pandas as pd


def is_positive(value):
    if value >= 0:
        return True
    else:
        return False


df = pd.read_csv("BQ_01-2022_03-05-2022.csv", sep=";", decimal=",")


df_2 = df[df["Montant de TVA total"] > 0]
tva_collectee = 0
tva_deductible = 0
for index, row in df_2.iterrows():
    if is_positive(row["Solde mouvement"]):
        tva_collectee += row["Montant de TVA total"]
    else:
        tva_deductible += row["Montant de TVA total"]

tva_due = tva_collectee - tva_deductible
print(f"TVA due {tva_due}")

